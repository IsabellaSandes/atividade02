package atividade.segunda.bancodedados.restapi;

import atividade.segunda.bancodedados.dao.PessoaDAO;
import java.util.List;
import org.springframework.web.bind.annotation.*;
import atividade.segunda.bancodedados.domain.Pessoa;

@RestController
public class PessoaApi {

    private PessoaDAO pessoaDAO = new PessoaDAO();

    //listar todas as pessoas que estao no banco
    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas(){
        return pessoaDAO.findAll();
    }

    //trazer pessoa com o id informado
    private PessoaDAO pessoaID = new PessoaDAO();
    @RequestMapping(value="/id/{numero}", method= RequestMethod.GET)
    public Pessoa findById(@PathVariable(value = "numero") Integer numero){
        return (Pessoa) pessoaID.findByID(numero);
    }

    //trazer pessoa que tenha o primeiro nome conforme o texto informado
    private PessoaDAO pessoaNome = new PessoaDAO();
    @RequestMapping(value="/nome/{primeiroNome}", method= RequestMethod.GET)
    public Pessoa findByName(@PathVariable(value = "primeiroNome") String primeiroNome){
        return (Pessoa) pessoaNome.findByName(primeiroNome);
    }

    //trazer lista de pessoas com pessoas que nasceram apos o ano informado
    private PessoaDAO pessoaAno = new PessoaDAO();
    @RequestMapping(value="/ano/{anoNascimento}", method= RequestMethod.GET)
    public List<Pessoa> findByAnoNasc(@PathVariable(value = "anoNascimento") Integer anoNascimento){
        return (List<Pessoa>) pessoaAno.findByAnoNasc(anoNascimento);
    }
}
